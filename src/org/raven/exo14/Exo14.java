package org.raven.exo14;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Exo14 {


	public static void main(String[] args) {
		
		List<String> words = Arrays.asList( "one", 
											"two", 
											"three",
											"four", 
											"five", 
											"six", 
											"seven", 
											"eight",
											"nine", 
											"ten", 
											"eleven",
											"twelve");
		
		//Question 1
		System.out.println("Question 1 : Elements du stream");
		words.stream()
		     .forEach(System.out::println);

		//Question 2
		System.out.println("\nQuestion 2 : Elements du stream en majuscule");
		words.stream()
			 .map(String::toUpperCase)
			 .forEach(System.out::println);
		
		//Question 3
		System.out.println("\nQuestion 3 : Premi�res lettres en majuscles, sans doublon");
		words.stream()
		 	 .map(s -> s.toUpperCase().substring(0, 1))
		 	 .distinct()
		 	 .forEach(System.out::println);
	
		//Question 4
		System.out.println("\nQuestion 4 : Longueur elements stream sans doublon");
		words.stream()
			 .map(String::length)
			 .distinct()
		 	 .forEach(System.out::println);
		
		//Question 5
		System.out.println("\nQuestion 5 :");
		long streamLength = words.stream()
						  		 .count();
		System.out.println("Ce stream contient " + streamLength + " elements\n");
	
		//Question 6
		System.out.println("\nQuestion 6 : ");
		long nbElPair = words.stream()
							 .filter(s -> s.length()%2 == 0)
							 .count();
		System.out.println("Ce stream contient " + nbElPair + " elements pairs");
		
		//Question 7
		System.out.println("\nQuestion 7 : ");
		int maxLength = words.stream()
									   .mapToInt(s -> s.length())
									   .max()
									   .orElseThrow();
		System.out.println("La longueur de la chaine la plus longue est : " + maxLength);
		
		//Question 8
		System.out.println("\nQuestion 8 : ");
		 List<String> listeImpaire = words.stream()
										  .map(s -> s.toUpperCase())
										  .filter(s -> s.length()%2 != 0)
										  .collect(
												Collectors.toList());
		
		 System.out.println("Liste de mots de longueur impaire = " + listeImpaire);
		
		
		//Question 9
		 System.out.println("\nQuestion 9 : Concatenation chaines de 5 caracteres ou moins ");
		String stringCat = words.stream()
								.filter(s -> s.length() <= 5)
								.sorted()
								.collect(Collectors.joining(", ", "{" ,"}"))
								;
		System.out.println(stringCat);
		
		//Question 10
		System.out.println("\nQuestion 10 : Table de hachage (K : longueur des chaines, V: listes de chaines de caract�res ");
		Map<Integer, List<String>> map1 = 
				words.stream()
					   .collect(
							   Collectors.groupingBy(
									   String::length,
									   Collectors.toList()
							   )
					   );
		map1.forEach((key, value) -> System.out.println(key + " = " + value));
		
		//Question 11
		System.out.println("\nQuestion 11 : Table de hachage (K : premi�re lettre, V : concat�nations des chaines) ");
		Map<String, String> map2 = 
						words.stream()	
							 .collect(
									   Collectors.groupingBy(
											   s -> s.substring(0,1),
											   Collectors.joining(", "))
									   );			  
		map2.forEach((key, value) -> System.out.println(key + " est associ� � la chaine : " + value));
	}

}
